class Trivium:
    """
    Classe permettant de debugger les differentes etapes du trivium
    """
    def __init__(self, key, iv):
        self.key = key
        self.iv = iv
        self.reg_1 = [] # [s1, ..., s93] 93 elts
        self.reg_2 = [] # [s94, ..., s177] 84 elts
        self.reg_3 = [] # [s178, ..., s288] 111 elts
        self.z = []

    def init_register(self):
        """
        fonction pour init les 3 registres en fonction de la clé et de l'iv donnés
        """

        self.reg_3 = [0]*108 + [1] * 3

        for i in range(93):
            if i < (80 - 1):
                self.reg_1.append(self.key[i])
            else:
                self.reg_1.append(0)

        for i in range(84):
            if i < (80 - 1):
                self.reg_2.append(self.key[i])
            else:
                self.reg_2.append(0)

    def first_salad(self):
        n = 4 * 288

        for i in range(n):
            t1 = self.reg_1[(66 - 1)] ^ self.reg_1[(91 - 1)] & \
                 self.reg_1[(92 - 1)] ^ self.reg_1[(93 - 1)] ^ self.reg_2[(171 - 1) % 93]

            t2 = self.reg_2[(162 - 1) % 93] ^ self.reg_2[(175 - 1) % 93] & \
                 self.reg_2[(176 - 1) % 93] ^ self.reg_2[(177 - 1) % 93] ^ self.reg_3[(264 - 1) % (93 + 84)]

            t3 = self.reg_3[(243 - 1) % (93 + 84)] ^ self.reg_3[(286 - 1) % (93 + 84)] & \
                 self.reg_3[(287 - 1) % (93 + 84)] ^ self.reg_3[(288 - 1) % (93 + 84)] & \
                 self.reg_1[(69 - 1)]

            self.reg_1.insert(1, t3)
            self.reg_1.pop()

            self.reg_2.insert(1, t1)
            self.reg_2.pop()

            self.reg_3.insert(1, t2)
            self.reg_3.pop()

        print("first salad done : ")
        print(self.reg_1)
        print(self.reg_2)
        print(self.reg_3)

    def second_salad(self, n):

        for i in range(n):
            t1 = self.reg_1[(66 - 1)] ^ self.reg_1[(93 - 1)]
            t2 = self.reg_2[(162 - 1) % 93] ^ self.reg_2[(177 - 1) % 93]
            t3 = self.reg_3[(243 - 1) % (93 + 84)] ^ self.reg_3[(288 - 1) % (93 + 84)]

            self.z.append(t1 ^ t2 ^ t3)

            t1 = t1 ^ self.reg_1[(66 - 1)] ^ self.reg_1[(91 - 1)] & \
                 self.reg_1[(92 - 1)] ^ self.reg_2[(171 - 1) % 93]

            t2 = t2 ^ self.reg_2[(175 - 1) % 93] & \
                 self.reg_2[(176 - 1) % 93] ^ self.reg_3[(264 - 1) % (93 + 84)]

            t3 = t3 ^ self.reg_3[(286 - 1) % (93 + 84)] & \
                 self.reg_3[(287 - 1) % (93 + 84)] ^ self.reg_1[(69 - 1)]

            self.reg_1.insert(1, t3)
            self.reg_1.pop()

            self.reg_2.insert(1, t1)
            self.reg_2.pop()

            self.reg_3.insert(1, t2)
            self.reg_3.pop()

        print("second salad done : ")
        print(self.z)

    def generate_key(self):
        self.init_register()
        print("reg_1 done : ", self.reg_1)
        print("reg_2 done : ", self.reg_2)
        print("reg_3 done : ", self.reg_3)
        print("------------------------")
        self.first_salad()
        print("------------------------")
        self.second_salad(100)

    def print_key_iv(self):
        print(self.key, " ", self.iv)

