from Trivium import Trivium

if __name__ == '__main__':
    iv = [0] * 80
    key = [0] * 80

    tr = Trivium(iv, key)
    tr.print_key_iv()
    tr.generate_key()

